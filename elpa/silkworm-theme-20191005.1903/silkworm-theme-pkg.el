;;; -*- no-byte-compile: t -*-
(define-package "silkworm-theme" "20191005.1903" "Light theme with pleasant, low contrast colors." '((emacs "24")) :commit "6cb44e3bfb095588aa3bdf8d0d45b583521f9e2c" :authors '(("Martin Haesler")) :maintainer '("Martin Haesler"))
