;;; purp-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "blurb-theme" "blurb-theme.el" (0 0 0 0))
;;; Generated autoloads from blurb-theme.el

(when load-file-name (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "blurb-theme" '("blurb")))

;;;***

;;;### (autoloads nil "purp-common" "purp-common.el" (0 0 0 0))
;;; Generated autoloads from purp-common.el

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "purp-common" '("purp-common-mktheme")))

;;;***

;;;### (autoloads nil "purp-light-theme" "purp-light-theme.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from purp-light-theme.el

(when load-file-name (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "purp-light-theme" '("purp-light")))

;;;***

;;;### (autoloads nil "purp-theme" "purp-theme.el" (0 0 0 0))
;;; Generated autoloads from purp-theme.el

(when load-file-name (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "purp-theme" '("purp")))

;;;***

;;;### (autoloads nil nil ("purp-theme-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; purp-theme-autoloads.el ends here
