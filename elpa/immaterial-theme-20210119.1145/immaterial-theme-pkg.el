(define-package "immaterial-theme" "20210119.1145" "A flexible theme based on material design principles"
  '((emacs "25"))
  :commit "2eec9998983bdbb2168f133c518143072c740a7f" :authors
  '(("Peter Gardfjäll"))
  :maintainer
  '("Peter Gardfjäll")
  :keywords
  '("themes")
  :url "https://github.com/petergardfjall/emacs-immaterial-theme")
;; Local Variables:
;; no-byte-compile: t
;; End:
