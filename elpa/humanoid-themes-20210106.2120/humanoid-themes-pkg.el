(define-package "humanoid-themes" "20210106.2120" "Color themes with a dark and light variant"
  '((emacs "24.3"))
  :commit "c1f9989bcecd1d93a2d7469d6b5c812bd35fe0f3" :authors
  '(("Thomas Friese"))
  :maintainer
  '("Thomas Friese")
  :keywords
  '("faces" "color" "theme")
  :url "https://github.com/humanoid-colors/emacs-humanoid-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
