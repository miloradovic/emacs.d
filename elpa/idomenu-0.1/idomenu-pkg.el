;;; -*- no-byte-compile: t -*-
(define-package "idomenu" "0.1" "imenu tag selection with ido" 'nil :commit "5daaf7e06e4704ae43c825488109d7eb8c049321" :keywords '("extensions" "convenience") :authors '(("Georg Brandl" . "georg@python.org")) :maintainer '("Georg Brandl" . "georg@python.org") :url "https://github.com/birkenfeld/idomenu")
