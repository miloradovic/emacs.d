;;; -*- no-byte-compile: t -*-
(define-package "qtcreator-theme" "20201215.1523" "A color theme that mimics Qt Creator IDE" '((emacs "24.3")) :commit "515532b05063898459157d2ba5c10ec0d5a4b1bd" :authors '(("Lesley Lai" . "lesley@lesleylai.info")) :maintainer '("Lesley Lai" . "lesley@lesleylai.info") :keywords '("theme" "light" "faces") :url "https://github.com/LesleyLai/emacs-qtcreator-theme")
