;;; -*- no-byte-compile: t -*-
(define-package "mandm-theme" "20191112.1832" "An M&M color theme." 'nil :commit "4e6ce4f222c1fa175d56e926628f37caa5f398ce" :authors '(("Christian Hopps" . "chopps@gmail.com")) :maintainer '("Christian Hopps" . "chopps@gmail.com") :url "https://github.com/choppsv1/emacs-mandm-theme.git")
