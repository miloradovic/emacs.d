;;; -*- no-byte-compile: t -*-
(define-package "paper-theme" "20200510.5" "A minimal Emacs colour theme." '((emacs "24")) :commit "e086c59a14701cd041641b51c952fa704ee963df" :keywords '("theme" "paper") :authors '(("Göktuğ Kayaalp")) :maintainer '("Göktuğ Kayaalp") :url "https://dev.gkayaalp.com/elisp/index.html#paper")
