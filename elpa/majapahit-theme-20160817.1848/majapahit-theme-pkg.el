(define-package "majapahit-theme" "20160817.1848" "Color theme with a dark and light versions" 'nil :commit "77c96df7619666b2102d90d452eeadf04adc89a6" :keywords
  ("color" "theme")
  :url "https://gitlab.com/franksn/majapahit-theme")
;; Local Variables:
;; no-byte-compile: t
;; End:
