;;; -*- no-byte-compile: t -*-
(define-package "labburn-theme" "20200822.2153" "A lab color space zenburn theme." 'nil :commit "4ef2892f56c973907361bc91495d14204744f678" :keywords '("theme" "zenburn") :authors '(("Johannes Goslar")) :maintainer '("Johannes Goslar") :url "https://github.com/ksjogo/labburn-theme")
