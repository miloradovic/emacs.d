;;; constant-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "constant-light-theme" "constant-light-theme.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from constant-light-theme.el

(when (and (boundp 'custom-theme-load-path) load-file-name) (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "constant-light-theme" '("constant-light")))

;;;***

;;;### (autoloads nil "constant-theme" "constant-theme.el" (0 0 0
;;;;;;  0))
;;; Generated autoloads from constant-theme.el

(when (and (boundp 'custom-theme-load-path) load-file-name) (add-to-list 'custom-theme-load-path (file-name-as-directory (file-name-directory load-file-name))))

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "constant-theme" '("constant")))

;;;***

;;;### (autoloads nil nil ("constant-theme-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; constant-theme-autoloads.el ends here
