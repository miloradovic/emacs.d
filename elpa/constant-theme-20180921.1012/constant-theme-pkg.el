(define-package "constant-theme" "20180921.1012" "A calm, dark, almost monochrome color theme."
  '((emacs "24.1"))
  :commit "23543a09729569b566175abe1efbe774048d3fa8" :keywords
  ("themes")
  :authors
  (("Jannis Pohlmann" . "contact@jannispohlmann.de"))
  :maintainer
  ("Jannis Pohlmann" . "contact@jannispohlmann.de")
  :url "https://github.com/jannis/emacs-constant-theme")
;; Local Variables:
;; no-byte-compile: t
;; End:
