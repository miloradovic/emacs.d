;;; -*- no-byte-compile: t -*-
(define-package "reykjavik-theme" "20201219.947" "Theme with a dark background." '((emacs "24")) :commit "f6d8e83946633603234cd1dac725e17447f40bce" :authors '(("martin haesler")) :maintainer '("martin haesler"))
