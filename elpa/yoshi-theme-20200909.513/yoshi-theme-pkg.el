;;; -*- no-byte-compile: t -*-
(define-package "yoshi-theme" "20200909.513" "Theme named after my cat" 'nil :commit "1ca48766209d941f0300d725ce9bec5a8bc2d642" :keywords '("faces") :authors '(("Tom Willemse" . "tom@ryuslash.org")) :maintainer '("Tom Willemse" . "tom@ryuslash.org") :url "http://projects.ryuslash.org/yoshi-theme/")
