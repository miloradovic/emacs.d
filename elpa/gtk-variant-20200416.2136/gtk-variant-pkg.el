;;; -*- no-byte-compile: t -*-
(define-package "gtk-variant" "20200416.2136" "Set the GTK theme variant (titlebar color)" '((emacs "25.1")) :commit "4462a5ab071ec001734e92d1ac2e5fa9721b94bd" :authors '(("Paul Oppenheimer")) :maintainer '("Paul Oppenheimer") :keywords '("frames" "gtk" "titlebar") :url "https://github.com/bepvte/gtk-variant.el")
