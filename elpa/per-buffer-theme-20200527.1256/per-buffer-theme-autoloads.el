;;; per-buffer-theme-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "per-buffer-theme" "per-buffer-theme.el" (0
;;;;;;  0 0 0))
;;; Generated autoloads from per-buffer-theme.el

(autoload 'per-buffer-theme/enable "per-buffer-theme" "\
Deprecated, please use `per-buffer-theme-mode'.

\(fn)" t nil)

(autoload 'per-buffer-theme/disable "per-buffer-theme" "\
Deprecated, please use `per-buffer-theme-mode'.

\(fn)" t nil)

(autoload 'per-buffer-theme-mode "per-buffer-theme" "\
Changes theme and/or font according to buffer name or major mode.

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "per-buffer-theme" '("per-buffer-theme/" "pbt~")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; per-buffer-theme-autoloads.el ends here
