;;; -*- no-byte-compile: t -*-
(define-package "undersea-theme" "20200719.618" "Theme styled after undersea imagery" '((emacs "24.3")) :commit "d4edb2cc110f1679ebc82cb52a4242cbc74636db" :authors '(("Shen, Jen-Chieh" . "jcs090218@gmail.com")) :maintainer '("Shen, Jen-Chieh" . "jcs090218@gmail.com") :url "https://github.com/jcs-elpa/undersea-theme")
