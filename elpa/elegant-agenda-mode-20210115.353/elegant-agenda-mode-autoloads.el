;;; elegant-agenda-mode-autoloads.el --- automatically extracted autoloads
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "elegant-agenda-mode" "elegant-agenda-mode.el"
;;;;;;  (0 0 0 0))
;;; Generated autoloads from elegant-agenda-mode.el

(autoload 'elegant-agenda-mode "elegant-agenda-mode" "\
Provides a more elegant view into your agenda

\(fn &optional ARG)" t nil)

(if (fboundp 'register-definition-prefixes) (register-definition-prefixes "elegant-agenda-mode" '("elegant-agenda-")))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; elegant-agenda-mode-autoloads.el ends here
