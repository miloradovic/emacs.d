;;; -*- no-byte-compile: t -*-
(define-package "elegant-agenda-mode" "20210115.353" "An elegant theme for your org-agenda" '((emacs "26.1")) :commit "5cbc688584ba103ea3be7d7b30e5d94e52f59eb6" :authors '(("Justin Barclay" . "justinbarclay@gmail.com")) :maintainer '("Justin Barclay" . "justinbarclay@gmail.com") :keywords '("faces") :url "https://github.com/justinbarclay/elegant-agenda-mode")
