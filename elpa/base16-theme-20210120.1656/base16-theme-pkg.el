(define-package "base16-theme" "20210120.1656" "Collection of themes built on combinations of 16 base colors" 'nil :commit "81ee33009584eff65e44cf6545e65c5e13298d00" :authors
  '(("Kaleb Elwert" . "belak@coded.io")
    ("Neil Bhakta"))
  :maintainer
  '("Kaleb Elwert" . "belak@coded.io")
  :url "https://github.com/belak/base16-emacs")
;; Local Variables:
;; no-byte-compile: t
;; End:
