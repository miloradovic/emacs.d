;;; -*- no-byte-compile: t -*-
(define-package "mlso-theme" "20200828.1221" "A dark, medium contrast theme" '((emacs "24")) :commit "b47243006470798caa4d3f8fe1af9bd5ef06bbee" :authors '(("github.com/Mulling")) :maintainer '("github.com/Mulling") :url "https://github.com/Mulling/mlso-theme")
