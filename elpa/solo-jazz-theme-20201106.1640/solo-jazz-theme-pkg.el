;;; -*- no-byte-compile: t -*-
(define-package "solo-jazz-theme" "20201106.1640" "The Solo-Jazz color theme" '((emacs "24.1")) :commit "3a2d1a0b404ba7c765526a1b76e0f1148ed8d0f2" :authors '(("Carl Steib")) :maintainer '("Carl Steib") :url "https://github.com/cstby/solo-jazz-emacs-theme")
