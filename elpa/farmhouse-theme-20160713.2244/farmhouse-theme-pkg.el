(define-package "farmhouse-theme" "20160713.2244" "Farmhouse Theme, Emacs edition" 'nil :commit "7ddc1ff13b4a3d5466bd0d33ecb86100352e83a7" :keywords
  ("color" "theme")
  :url "https://github.com/mattly/emacs-farmhouse-theme")
;; Local Variables:
;; no-byte-compile: t
;; End:
