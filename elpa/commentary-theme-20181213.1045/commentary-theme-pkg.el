;;; -*- no-byte-compile: t -*-
(define-package "commentary-theme" "20181213.1045" "A minimal theme with contrasting comments" '((emacs "24")) :commit "dede0f8ecb72156fa6ae81198ea570ead02997ff" :url "https://github.com/pzel/commentary-theme")
