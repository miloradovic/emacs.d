;;; -*- no-byte-compile: t -*-
(define-package "inverse-acme-theme" "20170823.254" "A theme that looks like an inverse of Acme's color scheme." '((autothemer "0.2") (cl-lib "0.5")) :commit "74d6f3e2f6534371509dd2d77006435156c276d6" :authors '(("Dylan Johnson")) :maintainer '("Dylan Johnson") :url "http://github.com/djohnson/inverse-acme-theme")
