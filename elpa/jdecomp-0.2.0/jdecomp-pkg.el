;;; -*- no-byte-compile: t -*-
(define-package "jdecomp" "0.2.0" "Interface to Java decompilers" '((emacs "24.5")) :commit "1590b06f139f036c1041e1ce5c0acccaa24b31a7" :keywords '("decompile" "java" "languages" "tools") :authors '(("Tianxiang Xiong" . "tianxiang.xiong@gmail.com")) :maintainer '("Tianxiang Xiong" . "tianxiang.xiong@gmail.com") :url "https://github.com/xiongtx/jdecomp")
