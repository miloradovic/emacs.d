;;; -*- no-byte-compile: t -*-
(define-package "hybrid-reverse-theme" "20210107.1435" "Emacs theme with material color scheme" '((emacs "24.1")) :commit "30072ccf0a49bb47360dce12965db1b1e2f2b57d" :authors '(("Riyyi")) :maintainer '("Riyyi") :keywords '("faces" "theme") :url "https://github.com/riyyi/emacs-hybrid-reverse")
