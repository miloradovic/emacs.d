;;; -*- no-byte-compile: t -*-
(define-package "prassee-theme" "20180709.1004" "A high contrast color theme for Emacs." '((emacs "24")) :commit "81126f69cdbaab836c00ae7a49aaf89d4229fde1" :keywords '("dark" "high-contrast" "faces") :authors '(("Prassee " . "prassee.sathian@gmail.com")) :maintainer '("Prassee " . "prassee.sathian@gmail.com") :url "https://github.com/prassee/prassee-emacs-theme")
