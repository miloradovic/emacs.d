;;; -*- no-byte-compile: t -*-
(define-package "laguna-theme" "20200928.2159" "A theme that's easy on the eyes & focuses on importance." 'nil :commit "61b18f6362b94e42ea5ab19a6f2debc2bd917eda" :authors '(("Henry Newcomer" . "a.cliche.email@gmail.com")) :maintainer '("Henry Newcomer" . "a.cliche.email@gmail.com") :url "https://github.com/HenryNewcomer/laguna-theme")
