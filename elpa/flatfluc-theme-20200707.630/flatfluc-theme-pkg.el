;;; -*- no-byte-compile: t -*-
(define-package "flatfluc-theme" "20200707.630" "Custom merge of flucui and flatui themes" '((emacs "26.1")) :commit "5a30b1cd344ac0d3c3bf9dab017805ab96897b54" :keywords '("lisp") :authors '(("Sébastien Le Maguer" . "lemagues@tcd.ie")) :maintainer '("Sébastien Le Maguer" . "lemagues@tcd.ie") :url "https://github.com/seblemaguer/flatfluc-theme")
