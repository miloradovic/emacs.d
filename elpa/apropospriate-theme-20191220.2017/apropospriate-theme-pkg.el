(define-package "apropospriate-theme" "20191220.2017" "A colorful, low-contrast, light & dark theme set for Emacs with a fun name." 'nil :commit "543341f0836b24e001375c530c4706e9345ec1e3" :keywords
  ("color" "theme")
  :url "https://github.com/waymondo/apropospriate-theme")
;; Local Variables:
;; no-byte-compile: t
;; End:
