;;; -*- no-byte-compile: t -*-
(define-package "load-theme-buffer-local" "20120702.2036" "Install emacs24 color themes by buffer." 'nil :commit "e606dec66f16a06140b9aad625a4fd52bca4f936" :keywords '("faces") :authors '(("Victor Borja" . "vic.borja@gmail.com")) :maintainer '("Victor Borja" . "vic.borja@gmail.com") :url "http://github.com/vic/color-theme-buffer-local")
