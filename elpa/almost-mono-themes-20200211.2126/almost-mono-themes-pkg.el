(define-package "almost-mono-themes" "20200211.2126" "Almost monochromatic color themes"
  '((emacs "24"))
  :commit "2f5935a1a9d042751c7135cac79875886edb2556" :keywords
  ("faces")
  :authors
  (("John Olsson" . "john@cryon.se"))
  :maintainer
  ("John Olsson" . "john@cryon.se")
  :url "https://github.com/cryon/almost-mono-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
