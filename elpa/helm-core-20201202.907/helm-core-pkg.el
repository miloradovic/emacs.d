(define-package "helm-core" "20201202.907" "Development files for Helm"
  '((emacs "25.1")
    (async "1.9.4"))
  :commit "4d44b3147941cfa4d1db3fb59d3506575b0ab24c" :url "https://emacs-helm.github.io/helm/")
;; Local Variables:
;; no-byte-compile: t
;; End:
