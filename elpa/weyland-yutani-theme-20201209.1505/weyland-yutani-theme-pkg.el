;;; -*- no-byte-compile: t -*-
(define-package "weyland-yutani-theme" "20201209.1505" "Emacs theme based off Alien movie franchise" '((emacs "24.1")) :commit "86c72ef8e3d25bee94c35254ad03b5d3e4836994" :authors '(("Joe Staursky")) :maintainer '("Joe Staursky") :url "https://github.com/jstaursky/weyland-yutani-theme")
