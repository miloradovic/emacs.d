(define-package "nubox" "20170619.910" "Nubox color theme (dark, light and tty versions)" 'nil :commit "1ccb8035ae42727ba6bdd5c1106fbceddeeed370" :keywords
  ("faces")
  :authors
  (("Martijn Terpstra" . "bigmartijn@gmail.com"))
  :maintainer
  ("Martijn Terpstra" . "bigmartijn@gmail.com"))
;; Local Variables:
;; no-byte-compile: t
;; End:
