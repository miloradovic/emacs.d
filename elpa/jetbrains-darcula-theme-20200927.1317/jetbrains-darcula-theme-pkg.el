;;; -*- no-byte-compile: t -*-
(define-package "jetbrains-darcula-theme" "20200927.1317" "A complete port of the default JetBrains Darcula theme" 'nil :commit "7a934115238d7b80df230a5ba7a70d866bc18c66" :authors '(("Ian Y.E. Pan")) :maintainer '("Ian Y.E. Pan") :url "https://github.com/ianpan870102/jetbrains-darcula-emacs-theme")
