;;; -*- no-byte-compile: t -*-
(define-package "jsx-mode" "0.1.10" "major mode for JSX" 'nil :commit "1ca260b76f6e6251c528ed89501597a5b456c179" :authors '(("Takeshi Arabiki (abicky)")) :maintainer '("Takeshi Arabiki (abicky)") :url "https://github.com/jsx/jsx-mode.el")
