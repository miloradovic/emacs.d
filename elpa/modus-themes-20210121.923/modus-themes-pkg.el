(define-package "modus-themes" "20210121.923" "Highly accessible themes (WCAG AAA)"
  '((emacs "26.1"))
  :commit "f0983fe9090c834d78a37bfbfe36c96dfd3b5f0b" :authors
  '(("Protesilaos Stavrou" . "info@protesilaos.com"))
  :maintainer
  '("Protesilaos Stavrou" . "info@protesilaos.com")
  :keywords
  '("faces" "theme" "accessibility")
  :url "https://gitlab.com/protesilaos/modus-themes")
;; Local Variables:
;; no-byte-compile: t
;; End:
