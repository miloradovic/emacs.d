;;; -*- no-byte-compile: t -*-
(define-package "rand-theme" "20151219.2335" "Random Emacs theme at start-up!" '((cl-lib "0.5")) :commit "65a00e5c5150f857aa96803b68f50bc8da0215b7" :authors '(("Daniel Gopar")) :maintainer '("Daniel Gopar") :url "https://github.com/gopar/rand-theme")
