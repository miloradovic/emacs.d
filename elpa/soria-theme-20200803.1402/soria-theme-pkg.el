;;; -*- no-byte-compile: t -*-
(define-package "soria-theme" "20200803.1402" "A xoria256 theme with some colors from openSUSE" '((emacs "25.1")) :commit "d5274cc4a8e19ed0f1393a09192def951d025a11" :keywords '("faces") :authors '(("Miquel Sabaté Solà" . "mikisabate@gmail.com")) :maintainer '("Miquel Sabaté Solà" . "mikisabate@gmail.com") :url "https://github.com/mssola/soria")
