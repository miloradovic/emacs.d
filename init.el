
;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

;;(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))

(add-to-list 'auto-mode-alist '("\\.js\\'" . rjsx-mode))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(custom-enabled-themes (quote (ample)))
 '(custom-safe-themes
   (quote
    ("b181ea0cc32303da7f9227361bb051bbb6c3105bb4f386ca22a06db319b08882" "36ca8f60565af20ef4f30783aa16a26d96c02df7b4e54e9900a5138fb33808da" "8f5b54bf6a36fe1c138219960dd324aad8ab1f62f543bed73ef5ad60956e36ae" "5e3fc08bcadce4c6785fc49be686a4a82a356db569f55d411258984e952f194a" "7356632cebc6a11a87bc5fcffaa49bae528026a78637acd03cae57c091afd9b9" "a707b3c5c02a8b31688a82125a4ce45d3f45514bcdaf1c8ac62b3473358d760a" "bf10bd6d21928bf87bc3032b498c62cb9d48c54c06d217c8b00bef8090e539f7" "387b487737860e18cbb92d83a42616a67c1edfd0664d521940e7fbf049c315ae" "3d4df186126c347e002c8366d32016948068d2e9198c496093a96775cc3b3eaa" default)))
 '(diary-entry-marker (quote font-lock-variable-name-face))
 '(emms-mode-line-icon-color "#358d8d")
 '(fci-rule-color "#f6f0e1")
 '(gnus-logo-colors (quote ("#259ea2" "#adadad")) t)
 '(gnus-mode-line-image-cache
   (quote
    (image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #358d8d\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };")) t)
 '(inhibit-startup-screen t)
 '(jdecomp-decompiler-paths
   (quote
    ((cfr . "~/tools/decompilers/cfr_0_132.jar")
     (fernflower . "~/idea-IC-162.1628.40/plugins/java-decompiler/lib/java-decompiler.jar")
     (procyon . "~/procyon-decompiler-0.5.30.jar"))))
 '(org-agenda-files (quote ("~/dusan/maximoplus/tasks.org")))
 '(package-selected-packages
   (quote
    (counsel counsel-web swiper color-theme-x elegant-agenda-mode gtk-variant theme-looper multiple-cursors acme-theme ahungry-theme ample-theme ample-zen-theme anti-zenburn-theme arc-dark-theme arjen-grey-theme atom-dark-theme autothemer avk-emacs-themes ayu-theme badger-theme berrys-theme birds-of-paradise-plus-theme blackboard-theme bliss-theme borland-blue-theme boron-theme brutal-theme brutalist-theme bubbleberry-theme busybee-theme calmer-forest-theme caroline-theme chocolate-theme circadian clues-theme colonoscopy-theme color-theme-sanityinc-solarized commentary-theme creamsody-theme cyberpunk-2019-theme cycle-themes dakrone-light-theme dakrone-theme danneskjold-theme darcula-theme dark-krystal-theme dark-mint-theme darkburn-theme darkokai-theme display-theme distinguished-theme django-theme doneburn-theme dracula-theme eclipse-theme ewal eziam-theme fantom-theme farmhouse-theme firecode-theme flatfluc-theme flucui-themes forest-blue-theme github-theme grayscale-theme green-is-the-new-black-theme hemisu-theme jetbrains-darcula-theme load-theme-buffer-local modus-operandi-theme modus-vivendi-theme monochrome-theme monokai-alt-theme per-buffer-theme phoenix-dark-pink-theme railscasts-theme rand-theme reverse-theme smart-mode-line-powerline-theme soft-charcoal-theme solo-jazz-theme subatomic256-theme sweet-theme tommyh-theme tramp-theme undersea-theme vscode-dark-plus-theme weyland-yutani-theme abyss-theme airline-themes alect-themes afternoon-theme almost-mono-themes apropospriate-theme atom-one-dark-theme autumn-light-theme badwolf-theme basic-theme cherry-blossom-theme chyla-theme cloud-theme constant-theme cyberpunk-theme darkmine-theme eink-theme espresso-theme ewal-doom-themes exotica-theme flatland-black-theme flatland-theme flatui-dark-theme flatui-theme foggy-night-theme gandalf-theme github-modern-theme goose-theme gotham-theme grandshell-theme green-phosphor-theme green-screen-theme greymatters-theme gruber-darker-theme habamax-theme hamburg-theme hc-zenburn-theme heaven-and-hell helm-themes hemera-theme heroku-theme horizon-theme hydandata-light-theme iceberg-theme idea-darkula-theme intellij-theme inverse-acme-theme iodine-theme ir-black-theme jazz-theme jbeans-theme klere-theme kooten-theme kosmos-theme lab-themes labburn-theme laguna-theme lavender-theme light-soap-theme liso-theme lush-theme madhat2r-theme majapahit-theme mandm-theme mbo70s-theme melancholy-theme mellow-theme metalheart-theme minimal-theme minsk-theme mlso-theme moe-theme molokai-theme monokai-pro-theme monotropic-theme mood-one-theme mustang-theme mustard-theme naquadah-theme naysayer-theme night-owl-theme noctilux-theme nofrils-acme-theme nord-theme northcode-theme nothing-theme nova-theme nubox nyx-theme obsidian-theme occidental-theme oceanic-theme oldlace-theme omtose-phellack-theme one-themes org-beautify-theme overcast-theme paganini-theme panda-theme paper-theme parchment-theme pastelmac-theme peacock-theme phoenix-dark-mono-theme plain-theme plan9-theme planet-theme poet-theme prassee-theme professional-theme punpun-theme purp-theme purple-haze-theme quasi-monochrome-theme railscasts-reloaded-theme rebecca-theme remember-last-theme rimero-theme seoul256-theme seti-theme sexy-monochrome-theme silkworm-theme slime-theme smart-mode-line-atom-one-dark-theme smyx-theme snazzy-theme soft-morning-theme soft-stone-theme soothe-theme soria-theme sourcerer-theme spacegray-theme spaceline-all-the-icons spacemacs-theme srcery-theme subatomic-theme sublime-themes sunny-day-theme suscolors-theme svg-mode-line-themes tango-2-theme tango-plus-theme tangotango-theme termbright-theme theme-magic toxi-theme tron-legacy-theme twilight-anti-bright-theme twilight-bright-theme twilight-theme ubuntu-theme ujelly-theme underwater-theme unobtrusive-magit-theme vs-dark-theme vs-light-theme vscdark-theme waher-theme warm-night-theme white-sand-theme white-theme xresources-theme yoshi-theme zen-and-art-theme zenburn-theme zeno-theme zerodark-theme zweilight-theme javascript ivy-explorer ivy gh-md jdecomp graphql-mode flx-ido ido-at-point ido-completing-read+ ido-vertical-mode idomenu smex expand-region magit prettier-js web-mode cider jsx-mode rjsx-mode paredit clojure-mode js2-mode)))
 '(red "#ffffff")
 '(tool-bar-mode nil))





(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
;; Comment/uncomment this line to enable MELPA Stable if desired.  See `package-archive-priorities`
;; and `package-pinned-packages`. Most users will not need or want to do this.
;;(add-to-list 'package-archives '("melpa-stable" . "https://stable.melpa.org/packages/") t)
(package-initialize)

(defun cljs-node-repl ()
  (interactive)
  (run-clojure "lein trampoline run -m clojure.main repl.clj"))

(defun cljs-old-repl ()
  (interactive)
  (run-clojure "lein trampoline cljsbuild repl-listen"))

(require 'prettier-js)

(defun set-auto-complete-as-completion-at-point-function ()
  (setq completion-at-point-functions '(auto-complete)))

(add-hook 'auto-complete-mode-hook 'set-auto-complete-as-completion-at-point-function)
(add-hook 'cider-mode-hook 'set-auto-complete-as-completion-at-point-function)
(add-hook 'js2-mode-hook 'prettier-js-mode)
(add-hook 'rjsx-mode-hook 'prettier-js-mode)
(add-hook 'web-mode-hook 'prettier-js-mode)
(add-hook 'rjsx-mode-hook 'prettier-js-mode)
(add-hook 'clojure-mode-hook           #'enable-paredit-mode)

(tool-bar-mode -1)


;; Functions (load all files in defuns-dir)
(setq defuns-dir (expand-file-name "defuns" user-emacs-directory))

(dolist (file (directory-files defuns-dir t "\\w+"))
  (when (file-regular-p file)
    (load file)))

(setq settings-dir
      (expand-file-name "settings" user-emacs-directory))

;; Set up load path
(add-to-list 'load-path settings-dir)

(require 'keybindings)

;;(eval-after-load 'ido '(require 'setup-ido))

(eval-after-load 'dired '(require 'setup-dired))

(jdecomp-mode 1)

(customize-set-variable 'jdecomp-decompiler-paths
                        '((cfr . "~/tools/decompilers/cfr_0_132.jar")
                          (fernflower . "~/idea-IC-162.1628.40/plugins/java-decompiler/lib/java-decompiler.jar")
                          (procyon . "~/procyon-decompiler-0.5.30.jar")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(require 'multiple-cursors)

(global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)

(global-set-key (kbd "C->") 'mc/mark-next-like-this)
(global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
(global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)



(ivy-mode 1)

(global-set-key (kbd "C-s") 'swiper-isearch)
(global-set-key (kbd "M-x") 'counsel-M-x)
(global-set-key (kbd "C-x C-f") 'counsel-find-file)
(global-set-key (kbd "M-y") 'counsel-yank-pop)
(global-set-key (kbd "<f1> f") 'counsel-describe-function)
(global-set-key (kbd "<f1> v") 'counsel-describe-variable)
(global-set-key (kbd "<f1> l") 'counsel-find-library)
(global-set-key (kbd "<f2> i") 'counsel-info-lookup-symbol)
(global-set-key (kbd "<f2> u") 'counsel-unicode-char)
(global-set-key (kbd "<f2> j") 'counsel-set-variable)
(global-set-key (kbd "C-x b") 'ivy-switch-buffer)
(global-set-key (kbd "C-c v") 'ivy-push-view)
(global-set-key (kbd "C-c V") 'ivy-pop-view)
